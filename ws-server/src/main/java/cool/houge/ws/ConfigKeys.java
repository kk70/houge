package cool.houge.ws;

/**
 * 配置键.
 * @author ZY (kzou227@qq.com)
 */
public final class ConfigKeys {

    /**
     * 应用名称.
     */
    public static final String APP_NAME = "app.name";
    /**
     * 服务主机.
     */
    public static final String SERVER_HOST = "server.host";
    /**
     * 服务端口.
     */
    public static final String SERVER_PORT = "server.port";
    /**
     * JWT密钥配置.
     */
    public static final String JWT_SECRETS = "jwt.secrets";

    /**
     * Kafka服务.
     */
    public static final String KAFKA_BOOTSTRAP_SERVERS = "kafka.bootstrap-servers";
    /**
     * Kafka订阅分组ID.
     */
    public static final String KAFKA_GROUP_ID = "kafka.group-id";
}
