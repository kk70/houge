package cool.houge.ws.main;

import io.avaje.inject.Bean;
import io.avaje.inject.Factory;
import io.avaje.jsonb.Jsonb;

/**
 *
 * @author ZY (kzou227@qq.com)
 */
@Factory
public class PrimaryBeanFactory {

    @Bean
    public Jsonb jsonb() {
        return Jsonb.builder().serializeEmpty(false).serializeNulls(false).build();
    }
}
