package cool.houge.ws.packet;

import io.avaje.jsonb.Json;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 错误包.
 *
 * @author ZY (kzou227@qq.com)
 */
@Json
@Data
@Accessors(chain = true)
public class ErrorPacket {

    /** 命名空间. */
    private String ns = "error";
    /** 错误代码. */
    private int code;
    /** 错误描述. */
    private String message;
}
