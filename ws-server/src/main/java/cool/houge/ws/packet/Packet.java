package cool.houge.ws.packet;

import io.avaje.jsonb.Json;
import io.avaje.jsonb.Json.Naming;
import java.util.List;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 消息包.
 *
 * @author ZY (kzou227@qq.com)
 */
@Data
@Accessors(chain = true)
@Json(naming = Naming.LowerUnderscore)
public class Packet {

    /** 私人消息. */
    public static final String NS_PRIVATE_MESSAGE = "p.message";
    /** 群组消息. */
    public static final String NS_GROUP_MESSAGE = "g.message";
    /** 群组消息订阅. */
    public static final String NS_GROUP_SUBSCRIBE = "g.subscribe";
    /** 取消群组消息订阅. */
    public static final String NS_GROUP_UNSUBSCRIBE = "g.unsubscribe";

    // ==========================================================================================//

    /** 消息命名空间. */
    private String ns;
    /** 消息ID. */
    private String messageId;

    /**
     * 群组 IDs.
     *
     * @see Packet#NS_GROUP_SUBSCRIBE
     * @see Packet#NS_GROUP_UNSUBSCRIBE
     */
    private List<String> groupIds;
    /**
     * 消息发送者.
     *
     * @see Packet#NS_PRIVATE_MESSAGE
     * @see Packet#NS_GROUP_MESSAGE
     */
    private String from;
    /**
     * 消息接收者.
     *
     * @see Packet#NS_PRIVATE_MESSAGE
     * @see Packet#NS_GROUP_MESSAGE
     */
    private String to;

    /**
     * 消息内容.
     *
     * @see Packet#NS_PRIVATE_MESSAGE
     * @see Packet#NS_GROUP_MESSAGE
     */
    private String content;
    /**
     * 扩展字段额外数据.
     *
     * @see Packet#NS_PRIVATE_MESSAGE
     * @see Packet#NS_GROUP_MESSAGE
     */
    private String extra;
}
