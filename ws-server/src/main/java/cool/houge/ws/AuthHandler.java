package cool.houge.ws;

import cool.houge.ws.session.Session;
import reactor.core.publisher.Mono;
import reactor.netty.http.websocket.WebsocketInbound;
import reactor.netty.http.websocket.WebsocketOutbound;

/**
 * @author ZY (kzou227@qq.com)
 */
@FunctionalInterface
public interface AuthHandler {

    /**
     * @param inbound
     * @param outbound
     * @return
     */
    Mono<Session> handle(WebsocketInbound inbound, WebsocketOutbound outbound);
}
