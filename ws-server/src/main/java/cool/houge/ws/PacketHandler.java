package cool.houge.ws;

import cool.houge.ws.packet.Packet;
import cool.houge.ws.session.Session;
import reactor.core.publisher.Mono;

/**
 * @author ZY (kzou227@qq.com)
 */
@FunctionalInterface
public interface PacketHandler {

    /**
     * @param session
     * @param packet
     * @return
     */
    Mono<Void> handle(Session session, Packet packet);
}
