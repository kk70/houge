package cool.houge.ws;

/**
 * @author ZY (kzou227@qq.com)
 */
public class BadAuthorizationException extends RuntimeException {

    public BadAuthorizationException(String message) {
        super(message);
    }
}
