package cool.houge.ws.impl;

import cool.houge.ws.ErrorHandler;
import cool.houge.ws.session.Session;
import jakarta.inject.Singleton;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import reactor.core.publisher.Mono;

/**
 * 错误处理.
 *
 * @author ZY (kzou227@qq.com)
 */
@Singleton
public class NothingErrorHandler implements ErrorHandler {

    private static final Logger log = LoggerFactory.getLogger(ErrorHandler.class);

    @Override
    public Mono<Void> handle(Session session, Throwable t) {
        log.error("{}", session, t);
        return Mono.empty();
    }
}
