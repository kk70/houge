package cool.houge.ws.impl;

import cool.houge.ws.PacketHandler;
import cool.houge.ws.packet.Packet;
import cool.houge.ws.session.Session;
import reactor.core.publisher.Mono;

/**
 * @author ZY (kzou227@qq.com)
 */
// @Singleton
public class NothingPacketHandler implements PacketHandler {

    @Override
    public Mono<Void> handle(Session session, Packet packet) {
        return session.send(packet);
    }
}
