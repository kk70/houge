package cool.houge.ws;

import cool.houge.ws.session.Session;
import reactor.core.publisher.Mono;

/**
 * @author ZY (kzou227@qq.com)
 */
public interface ErrorHandler {

    /**
     * @param session
     * @param t
     * @return
     */
    Mono<Void> handle(Session session, Throwable t);
}
