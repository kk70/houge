package cool.houge.ws;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import net.datafaker.Faker;

/**
 * 单元测试基础类.
 *
 * @author ZY (kzou227@qq.com)
 */
public abstract class TestBase {

    /**
     * 假数据生成器.
     */
    public static final Faker faker = new Faker();
    /**
     * 配置.
     */
    public static final Config config = ConfigFactory.load("houge-test.conf");
}
