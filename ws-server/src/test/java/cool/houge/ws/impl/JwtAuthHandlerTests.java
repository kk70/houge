package cool.houge.ws.impl;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

import cool.houge.ws.BadAuthorizationException;
import cool.houge.ws.TestBase;
import cool.houge.ws.jjwt.JsonbSerializer;
import io.jsonwebtoken.JwsHeader;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;
import io.netty.handler.codec.http.DefaultHttpHeaders;
import io.netty.handler.codec.http.HttpHeaderNames;
import java.nio.charset.StandardCharsets;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import reactor.netty.http.server.HttpServerRequest;
import reactor.netty.http.websocket.WebsocketInbound;
import reactor.netty.http.websocket.WebsocketOutbound;
import reactor.test.StepVerifier;

/**
 * @author ZY (kzou227@qq.com)
 */
class JwtAuthHandlerTests extends TestBase {

    JwtAuthHandler handler;

    @BeforeEach
    void beforeEach() {
        handler = new JwtAuthHandler(config);
    }

    @Test
    void handle() {
        var headers = new DefaultHttpHeaders();
        var key = Keys.hmacShaKeyFor(
                "3zzc2N6EEoMG7Yo1iK63equUztO8GZoG5y772wZb1XEtfD2kog3DBut5CPh8nroq7TteHLuCKci1DgBmsTdwFHousbA2R43nj8XPPPUauCz5XX70CHpYsNkBk0xvDuRZ"
                        .getBytes(StandardCharsets.UTF_8));
        var token = Jwts.builder()
                .serializeToJsonWith(new JsonbSerializer())
                .setHeaderParam(JwsHeader.KEY_ID, "INTEGRATION-TEST")
                .setSubject("1")
                .signWith(key)
                .compact();

        var inbound = mock(WebsocketInbound.class, withSettings().extraInterfaces(HttpServerRequest.class));
        doReturn(headers).when(inbound).headers();
        doReturn("?access_token=" + token).when((HttpServerRequest) inbound).uri();

        var outbound = mock(WebsocketOutbound.class);
        var p = handler.handle(inbound, outbound);
        StepVerifier.create(p)
                .consumeNextWith(session -> {
                    assertThat(session.uid()).isEqualTo("1");
                    assertThat(session.token()).isEqualTo(token);
                    assertThat(session.sessionId()).isPositive();
                })
                .expectComplete()
                .verify();
    }

    @Test
    void resolve() {
        var key = Keys.hmacShaKeyFor(
                "3zzc2N6EEoMG7Yo1iK63equUztO8GZoG5y772wZb1XEtfD2kog3DBut5CPh8nroq7TteHLuCKci1DgBmsTdwFHousbA2R43nj8XPPPUauCz5XX70CHpYsNkBk0xvDuRZ"
                        .getBytes(StandardCharsets.UTF_8));
        var token = Jwts.builder()
                .serializeToJsonWith(new JsonbSerializer())
                .setHeaderParam(JwsHeader.KEY_ID, "INTEGRATION-TEST")
                .setSubject("1")
                .signWith(key)
                .compact();

        var p = handler.resolve(token);
        StepVerifier.create(p)
                .consumeNextWith(t -> {
                    assertThat(t.getT1()).isEqualTo("1");
                    assertThat(t.getT2()).isEqualTo(token);
                })
                .expectComplete()
                .verify();
    }

    @Test
    void getToken_authorization_header() {
        var token = faker.internet().uuid();
        var headers = new DefaultHttpHeaders().add(HttpHeaderNames.AUTHORIZATION, "Bearer " + token);

        var inbound = mock(WebsocketInbound.class, withSettings().extraInterfaces(HttpServerRequest.class));
        doReturn(headers).when(inbound).headers();

        var p = handler.getToken(inbound);
        StepVerifier.create(p).expectNext(token).expectComplete().verify();
    }

    @Test
    void getToken_authorization_header_BadAuthorizationException() {
        var headers = new DefaultHttpHeaders().add(HttpHeaderNames.AUTHORIZATION, "Bearer");

        var inbound = mock(WebsocketInbound.class, withSettings().extraInterfaces(HttpServerRequest.class));
        doReturn(headers).when(inbound).headers();

        var p = handler.getToken(inbound);
        StepVerifier.create(p).expectError(BadAuthorizationException.class).verify();
    }

    @Test
    void getToken_authorization_header_BadAuthorizationException2() {
        var headers = new DefaultHttpHeaders()
                .add(
                        HttpHeaderNames.AUTHORIZATION,
                        "Bearer2 " + faker.internet().uuid());

        var inbound = mock(WebsocketInbound.class, withSettings().extraInterfaces(HttpServerRequest.class));
        doReturn(headers).when(inbound).headers();

        var p = handler.getToken(inbound);
        StepVerifier.create(p).expectError(BadAuthorizationException.class).verify();
    }

    @Test
    void getToken_access_token_query() {
        var headers = new DefaultHttpHeaders();
        var token = faker.internet().uuid();

        var inbound = mock(WebsocketInbound.class, withSettings().extraInterfaces(HttpServerRequest.class));
        doReturn(headers).when(inbound).headers();
        doReturn("?access_token=" + token).when((HttpServerRequest) inbound).uri();

        var p = handler.getToken(inbound);
        StepVerifier.create(p).expectNext(token).expectComplete().verify();
    }

    @Test
    void getToken_empty() {
        var headers = new DefaultHttpHeaders();

        var inbound = mock(WebsocketInbound.class, withSettings().extraInterfaces(HttpServerRequest.class));
        doReturn(headers).when(inbound).headers();
        doReturn("").when((HttpServerRequest) inbound).uri();

        var p = handler.getToken(inbound);
        StepVerifier.create(p).expectComplete().verify();
    }
}
