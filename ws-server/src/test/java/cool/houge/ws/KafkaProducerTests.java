package cool.houge.ws;

import cool.houge.lang.NanoIdUtils;
import cool.houge.ws.packet.Packet;
import io.avaje.jsonb.Jsonb;
import java.util.Map;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.ByteArraySerializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

/**
 * @author ZY (kzou227@qq.com)
 */
class KafkaProducerTests extends TestBase {

    @Disabled
    @Test
    void produce() {
        var opts = Map.<String, Object>of(
                ProducerConfig.BOOTSTRAP_SERVERS_CONFIG,
                config.getString("kafka.bootstrap-servers"),
                ProducerConfig.ACKS_CONFIG,
                "all",
                ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG,
                StringSerializer.class,
                ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG,
                ByteArraySerializer.class);

        try (var producer = new KafkaProducer<String, byte[]>(opts)) {
            var packet = new Packet()
                    .setNs(Packet.NS_PRIVATE_MESSAGE)
                    .setFrom("0")
                    .setTo("1")
                    .setContent(String.join(" ", faker.lorem().paragraphs(5)));

            var jsonb =
                    Jsonb.builder().serializeEmpty(false).serializeNulls(false).build();
            var value = jsonb.toJsonBytes(packet);

            producer.send(new ProducerRecord<>("messenger-transmit", NanoIdUtils.randomNanoId(), value));
        }
    }
}
